# Front lens imager


## One-Sentence-Pitch

Helps you keep a close eye on your objectives. 

## How does it work?

A common task performed by imaging facilities worldwide is to verify the condition of microscope objectives. Typically, the initial step involves unscrewing the objectives and visually examining the front lenses for any scratches or contamination. However, this process is time-consuming and carries a certain level of risk, as valuable objectives can easily be dropped during the procedure. To address this challenge, I have developed a stage insert that incorporates a simple USB endoscope camera (10 mm diameter, Teslong), along with a lens (EdmundOptics #63541 or #63-477, 10.0mm Dia. x 50.0mm FL, Uncoated, Plano-Convex Lens), and a mirror (EdmundOptics #32944, 20mm Dia Enhanced Aluminum, 4-6λ Mirror).

### **Update**: The [inverted version](#inverted-front-lens-imager) and the [the external stand](#external-stand) are the most useful in practice!


<img src="fullupright.jpg" width=80% />
<img src="endoscopeInUpright.jpg" width=80% />
<img src="crosssection.jpg" width=80% />

A white light LED positioned on the opposite side of the camera illuminates the front lenses of the objectives, and the resulting reflection is captured by the endoscope. This method allows for quick inspection of all front lenses by rotating the objective revolver and refocusing, eliminating the need to remove the objectives. Additionally, the inspection insert can be flipped, making it suitable for both inverted and upright microscopes.

<img src="fullinverted.jpg" width=80% />

The acquired images can be utilized to document the condition of an objective, eliminating the need for evaluating the objective's performance every time a scratch is (re-)discovered. Additionally, a time-lapse can be created from these images to monitor the deterioration of the objective over time.

This front lens imager, which is based on an endoscope camera, offers several advantages. It is highly cost-effective and easy to construct, with a budget of less than 80€. Moreover, it features built-in illumination and can be used with both upright and inverted microscopes. Hence, the objective can be illuminated either from the side using the battery-powered LED or by utilizing the integrated lights in 'reflection mode'. Furthermore, it securely clamps into the insert by hooking underneath it.

However, it is important to note that the camera's low resolution necessitates a small distance between the device and the objective lens. This makes focusing challenging and requires significant training to effectively utilize the tool. Another drawback is that it relies on a consumer camera with no guarantee of future availability. 

# Inverted front lens imager

This experience motivated me to develop a second, more advanced version that utilizes a recently available industrial camera called uEye-xc, which can be found at the following link: [uEye-xc]( https://en.ids-imaging.com/ueye-xc-autofocus-camera.html). This camera offers exceptional image quality and a highly reliable autofocus function (which can, as all other settings as well, be manually overwritten).

To assess the quality of the images, I encourage you to take a look at the [demo images folder](./DemoImages). Please keep in mind that the live images are easier to interpret and analyze.

Due to the significantly higher resolution of the uEye-xc camera compared to the endoscopic camera, I was able to design the front-lens imager in the form factor of a standard microscopy slide. This change enhances usability by making it more intuitive, requiring less explanation and training.

It looks like this:

<img src="DemoAssemblyInvertedSystems.jpg" width=80% />
<img src="DemoAssemblyInvertedSystemsBack.jpg" width=80% />
<img src="inInverted2.jpg" width=80% />


## Status

Both versions are already very helpful. Currently, I am working on a manual for the endoscope-based version, as well as a description and components list for the LED light (or a suitable pre-made alternative). The inverted XC camera version is functioning exceptionally well. To accommodate potential space limitations above or below the stage in your system, I have created two variants of the upright version.


<img src="DemoAssemblyUpright.jpg" width=80% />
<img src="DemoAssemblyUprightVariant.jpg" width=80% />

In either case, the inverted version profits from buying such a cable as the original usb connector is bulky and the original cable is very stiff:
 
<img src="flexcable.jpg" width=40% />
<img src="uprightwithflexcable.jpg" width=80% />

However, I was unable to provide sufficient power from the laptop to the camera using the flex cable and a USB extension. Consequently, I had to directly connect the flex cable to a powered USB hub.


# External stand

Additionally, I have developed an  [external stand](./ExternalStand/) that enables the imaging of unmounted objectives. It also proves useful for upright systems in cases where there is insufficient space for the upright mount:

<img src="externalStandModel.jpg" width=40% />
<img src="./ExternalStand/pictureofthestand.jpg" width=40% />

The objective is placed into a socket, which elevates it to the correct height and aligns the front lens at the center of the field of view. This streamlined process allows imaging to be completed in just 5 seconds: position the objective onto the socket, insert the socket into the stand until it touches, and capture a picture. The stand is designed for use atop an optical table, featuring (4mm diameter) magnets at its base to secure it and the objective in place. So far, I have created sockets compatible with a wide range of Olympus/Nikon/Zeiss (xxx.stl), and Leica(leicaxxx.stl) models. If you require a different height, simply adjust the socket's height non-uniformly using your slicer software.. 

Added a socket with an integrated light to image the front lens in transmission:

<img src="./ExternalStand/TransmissionStand.jpg" width=40% />


## Acknowledgment

This project has been made possible in part by grant number 2020-225401 from the Chan Zuckerberg Initiative DAF, an advised fund of Silicon Valley Community Foundation.